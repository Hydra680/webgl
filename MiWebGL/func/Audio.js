class Sound{
    constructor(src){
        this.src = src;
        this.audio = new Audio();
        this.audio.src = src;
        this.audio.loop = true;
    }
    play(){
        this.audio.play();
    }
    stop(){
        this.audio.pause();
    }
}