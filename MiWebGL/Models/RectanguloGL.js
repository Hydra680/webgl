class RectanguloGrafico {
    constructor(gl) {
      /**
       *    3 ---------- 2
       *     |        / |
       *     |      /   |
       *     |    /     |
       *     | /        |
       *    0 ---------- 1
       */

      /* Las coordenadas cartesianas (x, y) */
      var vertices = [
         0, 0, // 0
         2, 0, // 1
         2, 1, // 2
         0, 1, // 3
      ];


      /* Se crea el objeto del arreglo de vértices (VAO) */
      this.rectanguloVAO = gl.createVertexArray();

      /* Se activa el objeto */
      gl.bindVertexArray(this.rectanguloVAO);


      /* Se genera un nombre (código) para el buffer */ 
      var codigoVertices = gl.createBuffer();

      /* Se asigna un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, codigoVertices);
   
      /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

      /* Se habilita el arreglo de los vértices (indice = 0) */
      gl.enableVertexAttribArray(0);

      /* Se especifica los atributos del arreglo de vértices */
      gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);


      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

      /* Se deja de asignar un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, null);

    }

    dibuja(gl) {

      /* Se activa el objeto del arreglo de vértices */
      gl.bindVertexArray(this.rectanguloVAO);

      /* Se renderiza las primitivas desde los datos del arreglo */
      gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);

      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

    }
  }