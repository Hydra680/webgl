class AutoGrafico {
    constructor(gl) {


        /* Las coordenadas cartesianas (x, y) */
        var vertices = [
            0.17*4, 0.0*4,      // 0
            -0.1*4, 0.007*4,    // 1
            -0.155*4,  0.054*4, // 5
            -0.159*4,0.075*4,   // 3
            -0.146*4,0.083*4,   // 4
            -0.144*4,0.139*4,   // 4
            -0.094*4,0.191*4,   // 6
            -0.112*4,0.207*4,   // 7
            -0.035*4,0.224*4,   // 8
            0.192*4,0.225*4,    // 9
            0.343*4,0.147*4,    // 10
            0.491*4,0.116*4,    // 11
            0.519*4,0.069*4,    // 12
            0.511*4,0.015*4,    // 13
            0.479*4,-0.004 *4   // 14
        ];
        var radio =0.05;
            
            for (var i = 0; i < 360; i++) {
               vertices.push(radio * Math.cos(i * Math.PI / 180));
               vertices.push(radio * Math.sin(i * Math.PI / 180));
            }

            radio = 0.05;
            for (var i = 0; i < 360; i++) {
               vertices.push((radio * Math.cos(i * Math.PI / 180)+0.4));
               vertices.push(radio * Math.sin(i * Math.PI / 180));
        }


        /* Se crea el objeto del arreglo de vértices (VAO) */
        this.rectanguloVAO = gl.createVertexArray();

        /* Se activa el objeto */
        gl.bindVertexArray(this.rectanguloVAO);


        /* Se genera un nombre (código) para el buffer */
        var codigoVertices = gl.createBuffer();

        /* Se asigna un nombre (código) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, codigoVertices);

        /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

        /* Se habilita el arreglo de los vértices (indice = 0) */
        gl.enableVertexAttribArray(0);

        /* Se especifica los atributos del arreglo de vértices */
        gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);


        /* Se desactiva el objeto del arreglo de vértices */
        gl.bindVertexArray(null);

        /* Se deja de asignar un nombre (código) al buffer */
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

    }

    dibuja(gl) {

        /* Se activa el objeto del arreglo de vértices */
        gl.bindVertexArray(this.rectanguloVAO);

        /* Se renderiza las primitivas desde los datos del arreglo */
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 15);
        // circulo 1
        gl.drawArrays(gl.TRIANGLE_FAN, 15, 360);
        
        // circulo 2
        gl.drawArrays(gl.TRIANGLE_FAN, 375, 360);

        /* Se desactiva el objeto del arreglo de vértices */
        gl.bindVertexArray(null);

    }
}