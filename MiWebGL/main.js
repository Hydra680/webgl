
//importar los archivos de la carpeta models

class Renderiza{
    //variable globales

    constructor(){

        
        this.xVelocidad_c1 =  0.1;  this.yVelocidad_c1 = 0;
        this.xVelocidad_c2 = -0.1;  this.yVelocidad_c2 = 0;
        this.xVelocidad_c3 =  0.03; this.yVelocidad_c3 = 0;

        this.xVelocidad_r1 =  0.05; this.yVelocidad_r1 = 0;
        this.xVelocidad_r2 = -0.05; this.yVelocidad_r2 = 0;
        this.xVelocidad_r3 = -0.03; this.yVelocidad_r3 = 0;

        /* Variables Uniformes */
        this.uColor;
        this.uMatrizProyeccion;
        this.uMatrizVista;
        this.uMatrizModelo;

        /* Matrices */
        this.MatrizProyeccion = new Array(16);
        this.MatrizVista = new Array(16);
        this.MatrizModelo = new Array(16);
        /* Paso 1: Se prepara el lienzo y se obtiene el contexto del WebGL.        */
        this.canvas = document.getElementById("webglcanvas");
        this.gl = this.canvas.getContext("webgl2");
        if (!this.gl) {
            document.write("WebGL 2.0 no está disponible en tu navegador");
            return;
        }
        this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);

        this.circulo1 = new CirculoGrafico(this.gl, 0.5);
        this.circulo2= new CirculoGrafico(this.gl, 0.5);;
        this.circulo3= new CirculoGrafico(this.gl, 0.5);;
        this.c1 = new Circulo(-2, 3, 0.5);;
        this.c2 = new Circulo(2, 3, 0.5);;
        this.c3 = new Circulo(-5, -2.5, 0.5);;

        this.rectangulo1= new RectanguloGrafico(this.gl);;
        this.rectangulo2= new RectanguloGrafico(this.gl);;
        this.rectangulo3= new RectanguloGrafico(this.gl);
        this.r3 = new Rectangulo(5, -3, 2, 1);;
        
        this.auto1 = new AutoGrafico(this.gl);
        this.r1 = new Rectangulo(-2, 0, 2.5, 1);;
        this.auto2 = new AutoGrafico(this.gl);
        this.r2 = new Rectangulo(2, 0, 2, 1);;
        /* Paso 2: Se crean, compilan y enlazan los programas Shader               */
        this.compilaEnlazaLosShaders();
    }
    
    compilaEnlazaLosShaders() {

        /* Se compila el shader de vertice */
        var shaderDeVertice = this.gl.createShader(this.gl.VERTEX_SHADER);
        this.gl.shaderSource(shaderDeVertice, document.getElementById("vs").text.trim());
        this.gl.compileShader(shaderDeVertice);
        if (!this.gl.getShaderParameter(shaderDeVertice, this.gl.COMPILE_STATUS)) {
            console.error(this.gl.getShaderInfoLog(shaderDeVertice));
        }

        /* Se compila el shader de fragmento */
        var shaderDeFragmento = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        this.gl.shaderSource(shaderDeFragmento, document.getElementById("fs").text.trim());
        this.gl.compileShader(shaderDeFragmento);
        if (!this.gl.getShaderParameter(shaderDeFragmento, this.gl.COMPILE_STATUS)) {
            console.error(this.gl.getShaderInfoLog(shaderDeFragmento));
        }

        /* Se enlaza ambos shader */
        this.programaID = this.gl.createProgram();
        this.gl.attachShader(this.programaID, shaderDeVertice); 
        this.gl.attachShader(this.programaID, shaderDeFragmento);
        this.gl.linkProgram(this.programaID);
        if (!this.gl.getProgramParameter(this.programaID, this.gl.LINK_STATUS)) {
            console.error(this.gl.getProgramInfoLog(this.programaID));
        }

        /* Se instala el programa de shaders para utilizarlo */
        this.gl.useProgram(this.programaID);
    }
}