@echo off
cls
"%APPDIR%chcp.com" 65001
title Cleaning
color 0A
@echo ███████████████████████████████
@echo ████╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬████
@echo ██╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬██
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬███████╬╬╬╬╬╬╬╬╬███████╬╬╬█
@echo █╬╬██╬╬╬╬███╬╬╬╬╬╬╬███╬╬╬╬██╬╬█
@echo █╬██╬╬╬╬╬╬╬██╬╬╬╬╬██╬╬╬╬╬╬╬██╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬╬█████╬╬╬╬╬╬╬╬╬╬╬█████╬╬╬╬█
@echo █╬╬█████████╬╬╬╬╬╬╬█████████╬╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬█╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬█╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬╬█╬╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo █╬╬╬▓▓▓▓╬╬╬╬╬╬╬█╬╬╬╬╬╬╬▓▓▓▓╬╬╬█
@echo █╬╬▓▓▓▓▓▓╬╬█╬╬╬█╬╬╬█╬╬▓▓▓▓▓▓╬╬█
@echo █╬╬╬▓▓▓▓╬╬██╬╬╬█╬╬╬██╬╬▓▓▓▓╬╬╬█
@echo █╬╬╬╬╬╬╬╬██╬╬╬╬█╬╬╬╬██╬╬╬╬╬╬╬╬█
@echo █╬╬╬╬╬████╬╬╬╬███╬╬╬╬████╬╬╬╬╬█
@echo █╬╬╬╬╬╬╬╬╬╬╬╬╬███╬╬╬╬╬╬╬╬╬╬╬╬╬█
@echo ██╬╬█╬╬╬╬╬╬╬╬█████╬╬╬╬╬╬╬╬█╬╬██
@echo ██╬╬██╬╬╬╬╬╬███████╬╬╬╬╬╬██╬╬██
@echo ██╬╬▓███╬╬╬████╬████╬╬╬███▓╬╬██
@echo ███╬╬▓▓███████╬╬╬███████▓▓╬╬███
@echo ███╬╬╬╬▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓╬╬╬╬███
@echo ████╬╬╬╬╬╬╬╬╬╬███╬╬╬╬╬╬╬╬╬╬████
@echo █████╬╬╬╬╬╬╬╬╬╬█╬╬╬╬╬╬╬╬╬╬█████
@echo ██████╬╬╬╬╬╬╬╬███╬╬╬╬╬╬╬╬██████
@echo ███████╬╬╬╬╬╬╬███╬╬╬╬╬╬╬███████
@echo ████████╬╬╬╬╬╬███╬╬╬╬╬╬████████
@echo █████████╬╬╬╬╬███╬╬╬╬╬█████████
@echo ███████████╬╬╬╬█╬╬╬╬███████████
@echo ███████████████████████████████
@echo by UsRex
pause
cls 

@echo tu direccion actual es 
@echo ██████████████████████████████████████████████████████████████
@echo "██████>>>> %cd% <<<<<██████"
@echo ██████████████████████████████████████████████████████████████
set /p direccion= [*]Que ruta desea limpiar:  
@echo %direccion%
cd /D %direccion%

@echo tu direccion actual es 
@echo ██████████████████████████████████████████████████████████████
@echo "██████>>>> %cd% <<<<<██████"
@echo ██████████████████████████████████████████████████████████████

@echo cambindo atributos de archivos
attrib /d /s -r -h -s *.*

@echo eliminando accesos directos
if exist *.ink del *.ink

@echo eliminando autorun
if exist autorun.inf del autorun.inf

@echo Operciones terminado con exito

@echo by UsRex
pause
exit 