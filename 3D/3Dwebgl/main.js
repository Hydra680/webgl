
//importar los archivos de la carpeta models

class Renderiza{
    //variable globales

    constructor(){
       
        /* Variables Uniformes */
        this.uColor;
        this.uMatrizProyeccion;
        this.uMatrizVista;
        this.uMatrizModelo;

        /* Matrices */
        this.MatrizProyeccion = new Array(16);
        this.MatrizVista = new Array(16);
        this.MatrizModelo = new Array(16);
        this.programaID;

        /* Paso 1: Se prepara el lienzo y se obtiene el contexto del WebGL.        */
        this.canvas = document.getElementById("webglcanvas");
        this.gl = this.canvas.getContext("webgl2");
        if (!this.gl) {
            document.write("WebGL 2.0 no está disponible en tu navegador");
            return;
        }
        this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);

         //se crean los objetos de la clas
        this.objeto = new Objeto(this.gl, "Modelos/cama.obj");
        this.objeto1 = new Objeto(this.gl, "Modelos/1.obj");
        this.objeto2 = new Objeto(this.gl, "Modelos/carpa.obj");
          
        //this.cubo = new Cubo(this.gl);
        this.esfera = new Esfera(this.gl, 1, 12, 12);
        this.rotX = 0;
        this.rotY = 0;
        this.rotZ = 0;
        this.INCX = 0.3;
        this.INCY = 0.2;
        this.INCZ = 0.4;
        this.incX = 0;
        this.incY = 0;
        this.incZ = 0;
 
        this.animacion = false;
 
        this.PERIODO_MOVIMIENTO = 0.01; // 1/60 = 0.0167 (60 cuadros por seg.)
        this.tiempo_real;
        this.tiempoMovimiento = this.PERIODO_MOVIMIENTO;
        this.inicio = Date.now(); // Tiempo Inicial
 

        /* Paso 2: Se crean, compilan y enlazan los programas Shader               */
        this.compilaEnlazaLosShaders();
    }
    
    compilaEnlazaLosShaders() {

        /* Se compila el shader de vertice */
        var shaderDeVertice = this.gl.createShader(this.gl.VERTEX_SHADER);
        this.gl.shaderSource(shaderDeVertice, document.getElementById("vs").text.trim());
        this.gl.compileShader(shaderDeVertice);
        if (!this.gl.getShaderParameter(shaderDeVertice, this.gl.COMPILE_STATUS)) {
            console.error(this.gl.getShaderInfoLog(shaderDeVertice));
        }

        /* Se compila el shader de fragmento */
        var shaderDeFragmento = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        this.gl.shaderSource(shaderDeFragmento, document.getElementById("fs").text.trim());
        this.gl.compileShader(shaderDeFragmento);
        if (!this.gl.getShaderParameter(shaderDeFragmento, this.gl.COMPILE_STATUS)) {
            console.error(this.gl.getShaderInfoLog(shaderDeFragmento));
        }

        /* Se enlaza ambos shader */
        this.programaID = this.gl.createProgram();
        this.gl.attachShader(this.programaID, shaderDeVertice); 
        this.gl.attachShader(this.programaID, shaderDeFragmento);
        this.gl.linkProgram(this.programaID);
        if (!this.gl.getProgramParameter(this.programaID, this.gl.LINK_STATUS)) {
            console.error(this.gl.getProgramInfoLog(this.programaID));
        }

        /* Se instala el programa de shaders para utilizarlo */
        this.gl.useProgram(this.programaID);
    }
}