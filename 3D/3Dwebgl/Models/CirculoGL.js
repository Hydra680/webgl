class CirculoGrafico {
    constructor(gl, radio) {

      /**
       *             3      2
       *             
       *       4                  1
       *        	
       *    5                         0
       *    
       *       6                  9
       *        
       *             7      8		
       */

      /* Las coordenadas cartesianas (x, y) */
      var vertices = [];

      /* Lee los vértices (x,y) y colores (r,g,b,a) */
      for (var i = 0; i < 360; i++) {
         vertices.push(radio * Math.cos(i * Math.PI / 180));
         vertices.push(radio * Math.sin(i * Math.PI / 180));
      }

      /* Se crea el objeto del arreglo de vértices (VAO) */
      this.circuloVAO = gl.createVertexArray();

      /* Se activa el objeto */
      gl.bindVertexArray(this.circuloVAO);


      /* Se genera un nombre (código) para el buffer */ 
      var codigoVertices = gl.createBuffer();

      /* Se asigna un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, codigoVertices);
   
      /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

      /* Se habilita el arreglo de los vértices (indice = 0) */
      gl.enableVertexAttribArray(0);

      /* Se especifica los atributos del arreglo de vértices */
      gl.vertexAttribPointer(0, 2, gl.FLOAT, false, 0, 0);


      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

      /* Se deja de asignar un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, null);

    }

    dibuja(gl) {

      /* Se activa el objeto del arreglo de vértices */
      gl.bindVertexArray(this.circuloVAO);

      /* Se renderiza las primitivas desde los datos del arreglo */
      gl.drawArrays(gl.TRIANGLE_FAN, 0, 360);

      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

    }
  }