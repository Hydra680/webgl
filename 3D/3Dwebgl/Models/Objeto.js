        /***********************************************************************************/
        /* Se define la geometría y se almacenan en los buffers de memoria y se renderiza. */
        /***********************************************************************************/

        class Objeto {
            constructor(gl, nombreArchivo) {
              var lineas, token, x, y, z, a, b;
              var minX, maxX, minY, maxY, minZ, maxZ;
              var numVertices;
  
              /* Las coordenadas cartesianas (x, y, z) */
              var vertices = [];
  
              /* Indices */
              var indices = [];
  
              /* Número de Vértices */
              numVertices = 0;
  
              /* Número de Triangulos */
              this.numLineas = 0;
  
              /* Lee el archivo .obj */
              var datos_obj = this.leeArchivo(nombreArchivo);
  
              /* Divide por lineas */
              lineas = datos_obj.split("\n");
  
              minX = Number.MAX_VALUE; maxX = Number.MIN_VALUE;
              minY = Number.MAX_VALUE; maxY = Number.MIN_VALUE;
              minZ = Number.MAX_VALUE; maxZ = Number.MIN_VALUE;
  
              for (let i = 0; i < lineas.length; i++) {
                this.inicia(lineas[i]); // Inicia el procesamiento de cadenas
                token = this.getToken();
                if (token != null) {
                  switch(token) {
                    case 'v': /* vértice */
                      x = this.getFloat();
                      y = this.getFloat();
                      z = this.getFloat();
                      vertices.push(x);
                      vertices.push(y);
                      vertices.push(z);
                      numVertices++;
  
                      minX = Math.min(minX, x); maxX = Math.max(maxX, x);
                      minY = Math.min(minY, y); maxY = Math.max(maxY, y);
                      minZ = Math.min(minZ, z); maxZ = Math.max(maxZ, z);
  
                      break;
                    case 'f': /* cara */
                      a = this.getInt()-1;
                      indices.push(a); // v0
  
                      this.numLineas++;
  
                      var tokenEntero = this.getInt();
  
                      while (tokenEntero != null) {
                        b = tokenEntero-1;
                        indices.push(b); // v1
                        indices.push(b); // v1
  
                        this.numLineas++;
  
                        tokenEntero = this.getInt();
                      }
  
                      indices.push(a); // v0
  
                      break;
                   }
                }
              }
  
              /* Redimensiona las coordenadas entre [-1,1] */
              var tam_max = 0, escala;
              tam_max = Math.max(tam_max, maxX-minX);
              tam_max = Math.max(tam_max, maxY-minY);
              tam_max = Math.max(tam_max, maxZ-minZ);
              escala = 2.0 / tam_max;
  
              /* Actualiza los vértices */
              for (var i = 0; i < numVertices * 3; i += 3) {
                vertices[i  ] = escala * (vertices[i  ] - minX) - 1.0;
                vertices[i+1] = escala * (vertices[i+1] - minY) - 1.0;
                vertices[i+2] = escala * (vertices[i+2] - minZ) - 1.0;
              }
  
              console.log(vertices.length);
              for (var i = 0; i < vertices.length; i+=3) {
                console.log(i + " : " + vertices[i] + "  " + vertices[i+1] + "  " + vertices[i+2])
              }
  
              console.log(indices.length);
              for (var i = 0; i < indices.length; i+=6) {
                console.log(i + " : " + indices[i] + "  " + indices[i+1] + "  " + indices[i+2] + "  " + indices[i+3] + "  " + indices[i+4] + "  " + indices[i+5])
              }
  
              /* Se crea el objeto del arreglo de vértices (VAO) */
              this.objetoVAO = gl.createVertexArray();
  
              /* Se activa el objeto */
              gl.bindVertexArray(this.objetoVAO);
  
  
              /* Se genera un nombre (código) para el buffer */ 
              var codigoVertices = gl.createBuffer();
  
              /* Se asigna un nombre (código) al buffer */
              gl.bindBuffer(gl.ARRAY_BUFFER, codigoVertices);
           
              /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
              gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
  
              /* Se habilita el arreglo de los vértices (indice = 0) */
              gl.enableVertexAttribArray(0);
  
              /* Se especifica el arreglo de vértices */
              gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
  
  
              /* Se genera un nombre (código) para el buffer */
              var codigoDeIndices = gl.createBuffer();
  
              /* Se asigna un nombre (código) al buffer */
              gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, codigoDeIndices);
  
              /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
              gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
  
  
              /* Se desactiva el objeto del arreglo de vértices */
              gl.bindVertexArray(null);
  
              /* Se deja de asignar un nombre (código) al buffer */
              gl.bindBuffer(gl.ARRAY_BUFFER, null);
  
              /* Se deja de asignar un nombre (código) al buffer */
              gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  
            }
  
            dibuja(gl) {
  
              /* Se activa el objeto del arreglo de vértices */
              gl.bindVertexArray(this.objetoVAO);
  
              /* Renderiza las primitivas desde los datos de los arreglos (vértices,
               * colores e indices) */
              gl.drawElements(gl.LINES, this.numLineas * 2, gl.UNSIGNED_SHORT, 0);
  
              /* Se desactiva el objeto del arreglo de vértices */
              gl.bindVertexArray(null);
  
            }
  
            /* Lee el archivo OBJ */
            leeArchivo(nombreArchivo){
              var byteArray = [];
              var request = new XMLHttpRequest();
              request.onreadystatechange = function () {
                if (request.readyState === 4 && request.status !== 404) {
                  byteArray = request.responseText
                }
              }
              request.open('GET', nombreArchivo, false); // Crea una solicitud para abrir el archivo
              request.send(null);                        // Enviando la solicitud
              return byteArray;
            }
  
            inicia(cadena) {
              this.cadena = cadena;
              this.indice = 0;
            }
  
            esDelimitador(c) {
              return (
                c == ' ' ||
                c == '\t' ||
                c == '(' ||
                c == ')' ||
                c == '"' ||
                c == "'"
             );
            }
  
            saltaDelimitadores() {
              while (this.indice < this.cadena.length && 
                       this.esDelimitador(this.cadena.charAt(this.indice))) {
                this.indice++;
              }
            };
  
            obtLongPalabra(pos) {
              var i = pos;
              while (i < this.cadena.length &&
                      !this.esDelimitador(this.cadena.charAt(i))) {
                   i++;
              }
              return i - pos;
            };
  
            getToken() {
              var n, subcadena;
              this.saltaDelimitadores();
              n = this.obtLongPalabra(this.indice);
              if (n === 0) {
                return null;
              }
              subcadena = this.cadena.substr(this.indice, n);
              this.indice = this.indice + (n + 1);
              return subcadena.trim();
            }
  
            getInt() {
              var token = this.getToken();
              if (token) {
                return parseInt(token, 10); // Retorna un número entero
              }
              return null;
            }
  
            getFloat() {
              var token = this.getToken();
              if (token) {
                return parseFloat(token); // Retorna un número en punto flotante
              }
              return null;
            }
          }