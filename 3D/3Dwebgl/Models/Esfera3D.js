class Esfera {

    /* segmentosH = slices o longitud, segmentosV = stacks o latitud  */
    constructor(gl, radio, segmentosH, segmentosV) {

      let cantidadDeVertices = (segmentosH+1)*(segmentosV+1);
      this.cantidadDeIndices = segmentosH * segmentosV * 6 * 2; // 6 vert (c/cuadrado)

      let i, j, k, x, y, z, theta_, phi_, k1, k2;

      /* Las coordenadas cartesianas (x, y) */
      let vertices = new Float32Array(cantidadDeVertices * 3);

      /* Indices */
      let indices = new Uint16Array(this.cantidadDeIndices); 

      /* Considere a las Coordenadas Esféricas para los siguientes cálculos */
      
      /* Se leen los vertices y las normales */
      k = 0;
      let theta = 2 * Math.PI / segmentosH; // 1 vuelta 360/segmentosH
      let phi = Math.PI / segmentosV;       // 1/2 vuelta 180/segmentosV

      // latitud
      for (i = 0; i <= segmentosV; i++) {
        phi_ = i * phi - Math.PI / 2; // -90..90 grados

        // longitud
        for (j = 0; j <= segmentosH; j++) {
          theta_ = j * theta; // 0..180 grados
          x = radio * Math.cos(theta_) * Math.cos(phi_);
          y = radio * Math.sin(theta_) * Math.cos(phi_);
          z = radio * Math.sin(phi_);

          //console.log(" theta: " + toDegrees(theta_) + " phi: " + toDegrees(phi_));

          vertices[k++] = x;
          vertices[k++] = y;
          vertices[k++] = z;
        }
      }

      /* Se leen los indices */
   
      /**
       *    k2 ------- k2+1
       *     |      /  | 
       *     |    /    |
       *     | /       |
       *    k1 ------- k1+1  
       *    k1---k2+1---k2   k1---k1+1---k2+1
       */
      k = 0;
      for(i = 0; i < segmentosV; i++) {
        k1 = i * (segmentosH + 1);      // inicio del actual segmentoV
        k2 = k1 + segmentosH + 1;       // inicio del siguiente segmentoV
        for (j = 0; j < segmentosH; j++) {
          indices[k++] = k1 + j;        // k1---k2+1---k2
          indices[k++] = k2 + j + 1;
          indices[k++] = k2 + j + 1;
          indices[k++] = k2 + j;
          indices[k++] = k2 + j;
          indices[k++] = k1 + j;

          indices[k++] = k1 + j;        // k1---k1+1---k2+1
          indices[k++] = k1 + j + 1;
          indices[k++] = k1 + j + 1;
          indices[k++] = k2 + j + 1;
          indices[k++] = k2 + j + 1;
          indices[k++] = k1 + j;
        }
      }

      /*
      console.log(vertices.length);
      for (i = 0; i < vertices.length; i+=3) {
        console.log(i + " : " + vertices[i] + "  " + vertices[i+1] + "  " + vertices[i+2])
      }
      console.log(indices.length);
      for (i = 0; i < indices.length; i+=6) {
        console.log(i + " : " + indices[i] + "  " + indices[i+1] + "  " + indices[i+2] + "  " + indices[i+3] + "  " + indices[i+4] + "  " + indices[i+5])
      }
      */

      /* Se crea el objeto del arreglo de vértices (VAO) */
      this.esferaVAO = gl.createVertexArray();

      /* Se activa el objeto */
      gl.bindVertexArray(this.esferaVAO);


      /* Se genera un nombre (código) para el buffer */ 
      var verticeBuffer = gl.createBuffer();

      /* Se asigna un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, verticeBuffer);
   
      /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

      /* Se habilita el arreglo de los vértices (indice = 0) */
      gl.enableVertexAttribArray(0);

      /* Se especifica el arreglo de vértices */
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);


      /* Se genera un nombre (código) para el buffer */
      var indiceBuffer = gl.createBuffer();

      /* Se asigna un nombre (código) al buffer */
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indiceBuffer);

      /* Se transfiere los datos desde la memoria nativa al buffer de la GPU */
      gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);


      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

      /* Se deja de asignar un nombre (código) al buffer */
      gl.bindBuffer(gl.ARRAY_BUFFER, null);

      /* Se deja de asignar un nombre (código) al buffer */
      gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    }

    dibuja(gl) {

      /* Se activa el objeto del arreglo de vértices */
      gl.bindVertexArray(this.esferaVAO);

      /* Renderiza las primitivas desde los datos de los arreglos (vértices,
       * normales e indices) */
      gl.drawElements(gl.LINES, this.cantidadDeIndices, gl.UNSIGNED_SHORT, 0);

      /* Se desactiva el objeto del arreglo de vértices */
      gl.bindVertexArray(null);

    }
  }
